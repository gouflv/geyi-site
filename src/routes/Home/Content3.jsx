import React from 'react';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';

class Content extends React.Component {

  static defaultProps = {
    className: 'content1',
  };

  render() {
    const props = { ...this.props };
    const isMode = props.isMode;
    delete props.isMode;
    const animType = {
      queue: isMode ? 'bottom' : 'left',
      one: isMode ? { y: '+=30', opacity: 0, type: 'from' }
        : { x: '+=30', opacity: 0, type: 'from' },
    };
    return (
      <div
        {...props}
        className={`content-template-wrapper content-half-wrapper ${props.className}-wrapper`}
      >
        <OverPack
          className={`content-template ${props.className}`}
          location={props.id}
        >
          <QueueAnim
            type={animType.queue}
            className={`${props.className}-text`}
            key="text"
            leaveReverse
            ease={['easeOutCubic', 'easeInCubic']}
            id={`${props.id}-textWrapper`}
          >
            <h1 key="h1" id={`${props.id}-title`}>
              MR创新实验室
            </h1>
            <p key="p" id={`${props.id}-content`}>
              <h3>构成：</h3>
              成体系的AR/VR探究案例 &nbsp;&nbsp;
              智能的学习反馈评价系统 &nbsp;&nbsp;
              探究式学习工具 &nbsp;&nbsp;
              智能多媒体教学机器人
              <br />
              <br />
              <h3>特性：</h3>
              真实和虚拟世界的信息集成 &nbsp;&nbsp;
              知识和实验的无缝整合
            </p>
          </QueueAnim>
          <TweenOne
            key="img"
            animation={animType.one}
            className={`${props.className}-img`}
            id={`${props.id}-imgWrapper`}
            resetStyleBool
          >
            <span id={`${props.id}-img`}>
              <img
                width="100%" src="http://cdn.geyitech.com/official/03_02
_0.png"
              />
            </span>
          </TweenOne>
        </OverPack>
      </div>
    );
  }
}

export default Content;
