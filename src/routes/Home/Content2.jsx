import React from 'react';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import OverPack from 'rc-scroll-anim/lib/ScrollOverPack';

class Content extends React.Component {
  static defaultProps = {
    className: 'content0',
  };

  render() {
    const props = { ...this.props };
    const isMode = props.isMode;
    delete props.isMode;
    const animType = {
      queue: isMode ? 'bottom' : 'right',
      one: isMode ? { y: '+=30', opacity: 0, type: 'from' }
        : { x: '-=30', opacity: 0, type: 'from' },
    };
    return (
      <div
        {...props}
        className={`content-template-wrapper content-half-wrapper ${props.className}-wrapper`}
      >
        <OverPack
          className={`content-template ${props.className}`}
          location={props.id}
        >
          <TweenOne
            key="img"
            animation={animType.one}
            className={`${props.className}-img`}
            id={`${props.id}-imgWrapper`}
            resetStyleBool
          >
            <span id={`${props.id}-img`}>
              <img width="100%" src="http://cdn.geyitech.com/official/03_01_0.png" />
            </span>
          </TweenOne>
          <QueueAnim
            className={`${props.className}-text`}
            type={animType.queue}
            key="text"
            leaveReverse
            ease={['easeOutCubic', 'easeInCubic']}
            id={`${props.id}-textWrapper`}
          >
            <h1 key="h1" id={`${props.id}-title`}>
              科普全景畅想互动机
            </h1>
            <p key="p" id={`${props.id}-content`}>
              <h3>构成：</h3>
                55寸十点触摸互动机  &nbsp;&nbsp;
                高级穿戴式VR终端设备 &nbsp;&nbsp;
                AR智能制作呈现设备
              <br />
              <br />
              <h3>特性：</h3>
                部署便捷 &nbsp;&nbsp;
                适用广泛 &nbsp;&nbsp;
                维护简便 &nbsp;&nbsp;
                专题资源
            </p>
          </QueueAnim>
        </OverPack>
      </div>
    );
  }
}


export default Content;
